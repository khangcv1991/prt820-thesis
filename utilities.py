import numpy as np
import tensorflow as tf
from sklearn.model_selection import StratifiedKFold
import math
import PIL


class Utilities:

    def parse_labels(labels_batch, num_classes):
        """
        Convert integers to one-hot vectors
        Parameters
        ----------
        labels_batch: list
            Batch of labels
        num_classes: int
            Number of classes/families
        Return
        ------
        y_batch: list of one-hot vectors
        """
        y_batch = []
        for label in labels_batch:
            y = np.zeros(num_classes)
            y[label] = 1
            y_batch.append(y)
        return y_batch


    def convert_mlw_to_img(self, bytes_filepath, width=28, height=28):
        """
        It represents the hexadecimal content as an image by interpreting every byte as the gray-level of one pixel in
        an image. The resulting images have very fine texture patterns and can be used to extract visual signatures
        for each malware family
        Parameters
        ----------
        bytes_filepath: str
            Filepath of the bytes file
        width: int
            Width of the resulting image
        height: int
            Height of the resulting image
        Return
        ------
        img: np.array
            Array-like structure containing the representation of the malware sample as a gray-scale image
        """

        with open(bytes_filepath) as hex_file:
            #Extract hex values
            hex_array = []
            for line in hex_file.readlines():
                hex_values = line.split()
                if len(hex_values) != 17:
                    continue
                hex_array.append(
                    [int(i, 16) if i != '??' else 0 for i in hex_values[1:]])
            hex_array = np.array(hex_array)

        #Convert to an image of a specific size
        if hex_array.shape[1] != 16:
            assert (False)
        b = int((hex_array.shape[0] * 16) ** (0.5))
        b = 2 ** (int(math.log(b) / math.log(2)) + 1)
        a = int(hex_array.shape[0] * 16 / b)
        hex_array = hex_array[:a * b / 16, :]
        im = np.reshape(hex_array, (a, b))

        if width is not None and height is not None:
            #Downsample
            im = PIL.Image.fromarray(np.uint8(im))
            im = im.resize((width, height), resample=PIL.Image.ANTIALIAS)
            im = np.array(im.getdata())

        return im


    def writeFileSection(destdir, sourcedir, fname, fsection, headAddress, tailAddress):
        sourceFile = sourcedir + "/" + os.path.splitext(fname)[0] + ".bytes"
        destFile = destdir + "/" + \
            os.path.splitext(os.path.basename(fname))[0] + ".bytes"

        Path(destdir).mkdir(parents=True, exist_ok=True)
        fbytes = open(sourceFile, "r")
        datafile = fbytes.readlines()
        fout = open(destFile, "w+")

        try:
            headAddress = int(headAddress, 16)
            tailAddress = int(tailAddress, 16)
        except:
            print(sourceFile)
                # print (headAddress + " --- " + tailAddress)
            print(headAddress)
            print(tailAddress)
            return

        print("processing: " + sourceFile)
        for line in datafile:
            tmp = int(line[0:8], 16)
            if tmp >= headAddress and tmp <= tailAddress:
                fout.write(line)
        fout.close()


    def readwriteFileSection(sourcedir, fname, fsection):
        #open asm file to find head address and tail adderss of the section
        headAddress = None
        tailAddress = None
        returnValues = []
        sourcefile = sourcedir + "/" + fname
        print(sourcefile)
        fbytes = open(os.path.splitext(sourcefile)[0] + ".bytes", "r")
        with open(sourcefile, encoding="ISO-8859-1") as f:
            datafile = f.readlines()
            for line in datafile:
                if fsection+":00" in line[0:15] and headAddress == None:
                    headAddress = re.findall(r'00[0-9A-F]+', line, re.I)[0]
                    continue
                if fsection+":00" in line[0:15] and headAddress != None:
                    tmp = re.findall(r'00[0-9A-F]+', line, re.I)[0]
                    if tmp != headAddress:
                        tailAddress = tmp
        try:
            if headAddress == None or tailAddress == None:
                raise AddressNull()
        except:
            print("This address null")
        returnValues = np.append(headAddress, tailAddress)
        return returnValues
    def convertAndSave(array, name, destDir):
        if array.shape[1] != 16:  # If not hexadecimal
            assert(False)
        b = int((array.shape[0]*16)**(0.5))
        b = 2**(int(np.log(b)/np.log(2))+1)
        a = int(array.shape[0]*16/b)
        array = array[:a*b//16, :]
        array = np.reshape(array, (a, b))
        im = Image.fromarray(np.uint8(array))
        im.save(destDir+'/'+name+'.png', "PNG")
        return im

    def grayImg(destDir,sourceFile,fname):
        f = open(sourceFile)
        array = []
        for line in f:
            xx = line.split()
            if len(xx) != 17:
                continue
            array.append([int(i, 16) if i != '??' else 0 for i in xx[1:]])
        plt.imshow(convertAndSave(np.array(array), fname,destDir))
        del array
        f.close()


    def classifyDatasource(dataSource, dirDest, dataLable):
        malwareDict = {1: "Ramnit", 2: "Lollipop", 3: "Kelihos_ver3", 4: "Vundo",
                    5: "Simda", 6: "Tracur", 7: "Kelihos_ver1", 8: "Obfuscator.ACY", 9: "Gatak"}
        root = "/Users/admin/Documents/cdu/Thesis/code/malware-classification"
        tlFile = "trainLabels.csv"
        dataSource = "train"

        #create dir regarding malware family
        for key, value in malwareDict.items():
            Path(root+"/" + value).mkdir(parents=True, exist_ok=True)
        #classify files from my
        with open(root + '/' + tlFile,'r') as infile:
            reader = csv.reader(infile)
            next(reader, None)
            #create dictionary from csv key-malwarename value-family number
            mydict = {rows[0]: rows[1] for rows in reader}

        #move file to family dir
        malfiles = os.listdir(root + "/" + dataSource)
        for fullname in malfiles:
            try:
                fname = os.path.splitext(fullname)[0]
                source = root + "/" + dataSource + "/" + fullname
                destination = root + "/" + \
                    malwareDict[int(mydict[fname])] + "/" + fullname
                # if not os.path.exists(destination): open(destination, "w").close()
                shutil.move(source, destination)
            except:
                print(source + " === " + destination)

    def moveBackToDatasource(dataDest,datalable):
        malwareDict = {1: "Ramnit", 2: "Lollipop", 3: "Kelihos_ver3", 4: "Vundo",
                    5: "Simda", 6: "Tracur", 7: "Kelihos_ver1", 8: "Obfuscator.ACY", 9: "Gatak"}
        root = dataDest
        tlFile = datalable
        dataSource = "train"
        destinationDir = root + "/" + dataSource
        for key,value in malwareDict.items():
            sourceDir = root + "/" + value
            malfiles = os.listdir(root + "/" + value)
            for fullname in malfiles:
                try:
                    fname = os.path.splitext(fullname)[0]
                    sourceDir = root + "/" + value 
                    # if not os.path.exists(destination): open(destination, "w").close()
                    shutil.move(sourceDir + "/" + fullname, destinationDir + "/" + fullname)
                except:
                    print(source + " === " + destination)

    def genImageFromSection(src,dataSource, fSection):
        malfiles = os.listdir(dataSource)
        #.data section
        destDir = src + "/" + fSection 
        Path(destDir).mkdir(parents=True, exist_ok=True)

        src = "/Users/admin/Documents/cdu/Thesis/code/malware-classification"
        dataSource = src + "/" + dataSource
        destDir = src + "/" + fSection  
        

        for fname in malfiles:
            if '.asm' != fname[-4:]:
                continue
            # fname = "HmNZrjpJtMXGi9xS6ukW.asm"
            sourceSecBytes = destDir + "/" + os.path.splitext(fname)[0] +".bytes"
            Path(destDir).mkdir(parents=True, exist_ok=True)
            headtail = readwriteFileSection( dataSource, fname, ".data")
            writeFileSection(destDir, dataSource, fname, ".data",headtail[0], headtail[1]  )
            grayImg(destDir,sourceSecBytes,os.path.splitext(fname)[0])
            if os.path.exists(sourceSecBytes):
                os.remove(sourceSecBytes)
            else:
                print( sourceSecBytes + " The file does not exist")