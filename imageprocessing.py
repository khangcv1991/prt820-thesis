#https://www.kaggle.com/c/malware-classification/data?select=train.7z
#https://github.com/pratikpv/malware_classification
#https://www.youtube.com/watch?v=VLQTRlLGz5Y
#https://www.tensorflow.org/tutorials/images/classification

import os
import shutil
import matplotlib.pyplot as plt
import numpy as np
import math
import random
import struct
import re
import csv
from enum import Enum
from PIL import Image, ImageDraw
from pathlib import Path


def writeFileSection(destdir, sourcedir, fname, fsection, headAddress, tailAddress):
    sourceFile = sourcedir + "/" + os.path.splitext(fname)[0] + ".bytes"
    destFile = destdir + "/" + \
        os.path.splitext(os.path.basename(fname))[0] + ".bytes"

    Path(destdir).mkdir(parents=True, exist_ok=True)
    fbytes = open(sourceFile, "r")
    datafile = fbytes.readlines()
    fout = open(destFile, "w+")

    try:
        headAddress = int(headAddress, 16)
        tailAddress = int(tailAddress, 16)
    except:
        print(sourceFile)
        # print (headAddress + " --- " + tailAddress)
        print(headAddress)
        print(tailAddress)
        return

    
    for line in datafile:
        tmp = int(line[0:8], 16)
        if tmp >= headAddress and tmp <= tailAddress:
            fout.write(line)
    fout.close()


def readFileSection(sourcedir, fname, fsection):
    #open asm file to find head address and tail adderss of the section
    headAddress = None
    tailAddress = None
    returnValues = None
    sourcefile = sourcedir + "/" + fname
    lsec = len(fsection)
    # print(sourcefile)
    fbytes = open(os.path.splitext(sourcefile)[0] + ".bytes", "r")
    with open(sourcefile, encoding="ISO-8859-1") as f:
        datafile = f.readlines()
        for line in datafile:
            if fsection+":" in line[0:14] and headAddress == None:
                headAddress = line[(1+lsec):(lsec +9)] #.idata:005431AC
                continue
            if fsection+":" in line[0:14] and headAddress != None:
                tmp = line[(1+lsec):(lsec +9)]
                if tmp != headAddress:
                    tailAddress = tmp
    
    returnValues = []
    returnValues = np.append(headAddress, tailAddress)
    return returnValues
def convertAndSave(array, name, destDir):
    if array.shape[1] != 16:  # If not hexadecimal
        assert(False)
    b = int((array.shape[0]*16)**(0.5))
    b = 2**(int(np.log(b)/np.log(2))+1)
    a = int(array.shape[0]*16/b)
    array = array[:a*b//16, :]
    array = np.reshape(array, (a, b))
    im = Image.fromarray(np.uint8(array))
    im.save(destDir+'/'+name+'.png', "PNG")
    return im
def grayImg(destDir,sourceFile,fname):
    f = open(sourceFile)
    array = []
    for line in f:
        xx = line.split()
        if len(xx) != 17:
            continue
        array.append([int(i, 16) if i != '??' else 0 for i in xx[1:]])
    plt.imshow(convertAndSave(np.array(array), fname,destDir))
    del array
    f.close()
def procedureConvertToGrayTextsection(sourceDir,destDir):
    malFiles = os.listdir(sourceDir)
    Path(destDir).mkdir(parents=True, exist_ok=True)

    for fname in malFiles:
        if '.asm' != fname[-4:]:
            continue
        flag = 0
        headtail = readFileSection(sourceDir, fname, ".text")
        if headtail[0] == None or headtail[1] == None:
            headtail = readFileSection(sourceDir, fname, "CODE")
            flag = 1
        if headtail[0] == None or headtail[1] == None:
            flag = 2
        if flag == 0:
            writeFileSection(destDir, sourceDir, fname,
                         ".text", headtail[0], headtail[1])
        if flag == 1: 
            writeFileSection(destDir, sourceDir, fname,
                         "CODE", headtail[0], headtail[1])
        sourceFile = sourceDir + "/" + fname
        if flag == 2:
            sourceSecBytes = sourceDir + "/" + os.path.splitext(fname)[0] + ".bytes"
        else:
            sourceSecBytes = destDir + "/" + os.path.splitext(fname)[0] + ".bytes"

        grayImg(destDir, sourceSecBytes, os.path.splitext(fname)[0])

        if os.path.exists(sourceSecBytes) and flag != 2:
            os.remove(sourceSecBytes)
        else:
            print(sourceSecBytes + " The file does not exist")
def procedureConvertToGrayFull(sourceDir,destDir):
    malFiles = os.listdir(sourceDir)
    Path(destDir).mkdir(parents=True, exist_ok=True)

    for fname in malFiles:
        if '.asm' != fname[-4:]:
            continue
        sourceSecBytes = sourceDir + "/" + os.path.splitext(fname)[0] + ".bytes"
        print("processing: " + sourceSecBytes)
        grayImg(destDir, sourceSecBytes, os.path.splitext(fname)[0])

        
def classifyDatasource(dataSource, dirDest, dataLable):
    malwareDict = {1: "Ramnit", 2: "Lollipop", 3: "Kelihos_ver3", 4: "Vundo",
                    5: "Simda", 6: "Tracur", 7: "Kelihos_ver1", 8: "Obfuscator.ACY", 9: "Gatak"}
    root = "/Users/admin/Documents/cdu/Thesis/code/malware-classification"
    tlFile = "trainLabels.csv"
    dataSource = "train"

        #create dir regarding malware family
    for key, value in malwareDict.items():
        Path(root+"/" + value).mkdir(parents=True, exist_ok=True)
    #classify files from my
    with open(root + '/' + tlFile,'r') as infile:
        reader = csv.reader(infile)
        next(reader, None)
        #create dictionary from csv key-malwarename value-family number
        mydict = {rows[0]: rows[1] for rows in reader}

        #move file to family dir
    malfiles = os.listdir(root + "/" + dataSource)
    for fullname in malfiles:
        try:
            fname = os.path.splitext(fullname)[0]
            source = root + "/" + dataSource + "/" + fullname
            destination = root + "/" + \
                malwareDict[int(mydict[fname])] + "/" + fullname
            # if not os.path.exists(destination): open(destination, "w").close()
            shutil.move(source, destination)
        except:
            print(source + " === " + destination)





# classifyDatasource("/Users/admin/Documents/cdu/Thesis/code/malware-classification/train","/Users/admin/Documents/cdu/Thesis/code/malware-classification", "/Users/admin/Documents/cdu/Thesis/code/malware-classification/trainLabels.csv" )

procedureConvertToGrayTextsection("/Users/admin/Documents/cdu/Thesis/code/malware-classification/test", "/Users/admin/Documents/cdu/Thesis/code/malware-classification/testSecImg")
# procedureConvertToGrayTextsection("/Users/admin/Documents/cdu/Thesis/code/malware-classification/Kelihos_ver1", "/Users/admin/Documents/cdu/Thesis/code/malware-classification/image/Kelihos_ver1")
# procedureConvertToGrayTextsection("/Users/admin/Documents/cdu/Thesis/code/malware-classification/Kelihos_ver3", "/Users/admin/Documents/cdu/Thesis/code/malware-classification/image/Kelihos_ver3")
# procedureConvertToGrayTextsection("/Users/admin/Documents/cdu/Thesis/code/malware-classification/Lollipop", "/Users/admin/Documents/cdu/Thesis/code/malware-classification/image/Lollipop")
# procedureConvertToGrayTextsection("/Users/admin/Documents/cdu/Thesis/code/malware-classification/Obfuscator.ACY", "/Users/admin/Documents/cdu/Thesis/code/malware-classification/image/Obfuscator.ACY")
# procedureConvertToGrayTextsection("/Users/admin/Documents/cdu/Thesis/code/malware-classification/Ramnit", "/Users/admin/Documents/cdu/Thesis/code/malware-classification/image/Ramnit")
# procedureConvertToGrayTextsection("/Users/admin/Documents/cdu/Thesis/code/malware-classification/Simda", "/Users/admin/Documents/cdu/Thesis/code/malware-classification/image/Simda")
# procedureConvertToGrayTextsection("/Users/admin/Documents/cdu/Thesis/code/malware-classification/Tracur", "/Users/admin/Documents/cdu/Thesis/code/malware-classification/image/Tracur")
# procedureConvertToGrayTextsection("/Users/admin/Documents/cdu/Thesis/code/malware-classification/Vundo", "/Users/admin/Documents/cdu/Thesis/code/malware-classification/image/Vundo")



procedureConvertToGrayFull("/Users/admin/Documents/cdu/Thesis/code/malware-classification/test", "/Users/admin/Documents/cdu/Thesis/code/malware-classification/testFImg")
# procedureConvertToGrayFull("/Users/admin/Documents/cdu/Thesis/code/malware-classification/Kelihos_ver1", "/Users/admin/Documents/cdu/Thesis/code/malware-classification/imageF/Kelihos_ver1")
# procedureConvertToGrayFull("/Users/admin/Documents/cdu/Thesis/code/malware-classification/Kelihos_ver3", "/Users/admin/Documents/cdu/Thesis/code/malware-classification/imageF/Kelihos_ver3")
# procedureConvertToGrayFull("/Users/admin/Documents/cdu/Thesis/code/malware-classification/Lollipop", "/Users/admin/Documents/cdu/Thesis/code/malware-classification/imageF/Lollipop")
# procedureConvertToGrayFull("/Users/admin/Documents/cdu/Thesis/code/malware-classification/Obfuscator.ACY", "/Users/admin/Documents/cdu/Thesis/code/malware-classification/imageF/Obfuscator.ACY")
# procedureConvertToGrayFull("/Users/admin/Documents/cdu/Thesis/code/malware-classification/Ramnit", "/Users/admin/Documents/cdu/Thesis/code/malware-classification/imageF/Ramnit")
# procedureConvertToGrayFull("/Users/admin/Documents/cdu/Thesis/code/malware-classification/Simda", "/Users/admin/Documents/cdu/Thesis/code/malware-classification/imageF/Simda")
# procedureConvertToGrayFull("/Users/admin/Documents/cdu/Thesis/code/malware-classification/Tracur", "/Users/admin/Documents/cdu/Thesis/code/malware-classification/imageF/Tracur")
# procedureConvertToGrayFull("/Users/admin/Documents/cdu/Thesis/code/malware-classification/Vundo", "/Users/admin/Documents/cdu/Thesis/code/malware-classification/imageF/Vundo")


# testDirreadFileSection("Gatak", "CODE")
# testDirreadFileSection("Ramnit", ".text")
# testDirreadFileSection("Lollipop", ".text")
# testDirreadFileSection("Kelihos_ver3", ".text")
# testDirreadFileSection("Vundo", ".text")
# testDirreadFileSection("Simda", ".text")
# testDirreadFileSection("Tracur", ".text")
# testDirreadFileSection("Kelihos_ver1", ".text")
# testDirreadFileSection("Obfuscator.ACY", ".text")
# testDirreadFileSection("Gatak", ".text")
# testreadFileSection()
# classifyDatasource(dataSource, dirDest, dataLable):