import os
import zipfile
import tensorflow as tf
from tensorflow.keras.optimizers import RMSprop
from tensorflow.keras.preprocessing.image import ImageDataGenerator
from tensorflow.keras import layers
from tensorflow.keras import Model
from tensorflow.keras.applications.inception_v3 import InceptionV3
from tensorflow.keras.optimizers import RMSprop
from keras.preprocessing.image import ImageDataGenerator
from sklearn.model_selection import train_test_split
import tensorflow as tf
from tensorflow.keras import datasets, layers, models
from sklearn.utils import class_weight
import numpy as np
import pathlib
from PIL import Image, ImageFile
import matplotlib.pyplot as plt
#dataset
imgh = 299
imgw = 299
batchs = 11000
ImageFile.LOAD_TRUNCATED_IMAGES = True
#Generating DataSet
path_root = "/Users/admin/Documents/cdu/Thesis/code/malware-classification/trainingImage/trainingimage"
batches = ImageDataGenerator().flow_from_directory(directory=path_root, target_size=(imgh,imgw), batch_size=batchs)
imgs, labels = next(batches)
# print(imgs/255.)
X_train, X_test, y_train, y_test = train_test_split(imgs,labels, test_size=0.3)
pre_trained_model = InceptionV3()

for layer in pre_trained_model.layers:
  layer.trainable = False
class myCallback(tf.keras.callbacks.Callback):
  def on_epoch_end(self, epoch, logs={}):
    if(logs.get('acc')>0.959):
      print("\nReached 99.9% accuracy so cancelling training!")
      self.model.stop_training = True

# Flatten the output layer to 1 dimension
x = layers.Flatten()(pre_trained_model.output)
# Add a fully connected layer with 1,024 hidden units and ReLU activation
x = layers.Dense(1024, activation='relu')(x)
# Add a dropout rate of 0.2
x = layers.Dropout(0.2)(x)                  
# Add a final sigmoid layer for classification
x = layers.Dense  (1, activation='sigmoid')(x)           

model = Model( pre_trained_model.input) 

model.compile(optimizer = RMSprop(lr=0.0001), 
              loss = 'binary_crossentropy', 
              metrics = ['acc'])

#reshape lable as 2D
# Our vectorized labels
# y_train = np.asarray(y_train).astype('float32').reshape((-1,1))
# X_test = np.asarray(X_test).astype('float32').reshape((-1,1))
callbacks = myCallback()
history = model.fit(X_train, y_train, validation_data=(X_test, y_test), epochs=10)
#show diagram
acc = history.history['acc']
val_acc = history.history['val_acc']
loss = history.history['loss']
val_loss = history.history['val_loss']

epochs = range(len(acc))

plt.plot(epochs, acc, 'bo', label='Training accuracy')
plt.plot(epochs, val_acc, 'b', label='Validation accuracy')
plt.title('Training and validation accuracy')

plt.figure()

plt.plot(epochs, loss, 'bo', label='Training Loss')
plt.plot(epochs, val_loss, 'b', label='Validation Loss')
plt.title('Training and validation loss')
plt.legend()

plt.show()

