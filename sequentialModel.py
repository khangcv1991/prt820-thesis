from keras.preprocessing.image import ImageDataGenerator
from sklearn.model_selection import train_test_split
import tensorflow as tf
from tensorflow.keras import datasets, layers, models
from sklearn.utils import class_weight
import numpy as np
import pathlib
from PIL import Image, ImageFile
ImageFile.LOAD_TRUNCATED_IMAGES = True
#Generating DataSet
path_root = "/Users/admin/Documents/cdu/Thesis/code/malware-classification/trainingImage/trainingimageF"
batches = ImageDataGenerator().flow_from_directory(directory=path_root, target_size=(64,64), batch_size=10000)
imgs, labels = next(batches)
# print(batches[0])
num_classes = 9
#Split into train and test
X_train, X_test, y_train, y_test = train_test_split(imgs/255.,labels, test_size=0.3)

# print(list(y_train))

def malware_model():
    Malware_model = models.Sequential()
    Malware_model.add(layers.Conv2D(30, kernel_size=(3, 3),
                     activation='relu',
                     input_shape=(64,64,3)))
    Malware_model.add(layers.MaxPooling2D(pool_size=(2, 2)))
    Malware_model.add(layers.Conv2D(15, (3, 3), activation='relu'))
    Malware_model.add(layers.MaxPooling2D(pool_size=(2, 2)))
    Malware_model.add(layers.Dropout(0.25))
    Malware_model.add(layers.Flatten())
    Malware_model.add(layers.Dense(128, activation='relu'))
    Malware_model.add(layers.Dropout(0.5))
    Malware_model.add(layers.Dense(50, activation='relu'))
    Malware_model.add(layers.Dense(num_classes, activation='softmax'))
    Malware_model.compile(loss='categorical_crossentropy', optimizer = 'adam', metrics=['accuracy'])
    return Malware_model
Malware_model = malware_model()
# y_train_new = np.argmax(y_train, axis=1)
#Deal with unbalanced Data
# class_weights = class_weight.compute_class_weight('balanced',
#                                                  np.unique(y_train_new),
#                                                  y_train_new)
#  #Train and test our model                        
# Malware_model.fit(X_train, y_train, validation_data=(X_test, y_test), epochs=10,  class_weight=class_weights)
Malware_model.fit(X_train, y_train, validation_data=(X_test, y_test), epochs=10)

# Malware_model.fit(
#   train_ds,
#   validation_data=val_ds,
#   epochs=epochs
# )

scores = Malware_model.evaluate(X_test, y_test)